# ivy-symfony-console.el

`ivy-symfony-console.el` can be used to interact with the Symfony console using Emacs. Simply call `ivy-symfony-console`, type the command you wish to execute and then it'll be run in a shell in the other window. Note that my usage of this may be different from yours; while I have included support for `doctrine:query:sql` and `doctrine:query:dql`, any other commands that require user input may not work. If you run across such a command, please let me know.

## Installation

To install:
1) Clone this repository
2) Run `M-x RET package-install-file` in Emacs and direct it to ivy-symfony-console.el
3) Add `(require 'ivy-symfony-console)` to your init file

## Images

Showing the commands in the minibuffer:
![Showing the commands in the minibuffer](images/ivy-symfony-console-minibuffer.png)

Executing a command:
![Executing a command](images/ivy-symfony-console-execute-command.png)
