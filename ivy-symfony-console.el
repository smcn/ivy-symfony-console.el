;;; ivy-symfony-console.el --- Execute commands from the Symfony console in the Ivy minibuffer.

;; Author: Stephen McNelly <stephenmcnelly@gmail.com>
;; URL: http://gitlab.com/smcn/ivy-symfony-console.el
;; Version: 0.0.1
;; Keywords: php, symfony, ivy

;; Package-Requires: ((ivy "0.10.0") (f "0.20.0"))

;; This file is NOT part of GNU Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; (define-key php-mode-map (kbd "C-c C-c") 'ivy-symfony-console)

;;; Code:

(require 'f)
(require 'ivy)

(defun remove-nth-element (nth list)
  "Remove a specific element (NTH) from a LIST.
Code found here:
https://emacs.stackexchange.com/questions/29786/how-to-remove-delete-nth-element-of-a-list/29791#29791"
  (if (zerop nth) (cdr list)
    (let ((last (nthcdr (1- nth) list)))
      (setcdr last (cddr last))
      list)))

(defun go-up-dir (dir)
  "Go up a directory.  Same as DIR/.."
  (setq list (split-string dir "/"))
  (string-join (remove-nth-element (- (length list) 2) list) "/"))

(defun recursively-check-for-file (file path)
  "Check PATH for FILE.  If not found, go up a directory and check again.  Return path to FILE if found."
  (catch 'not-found
    (when (equal (length (split-string path "/")) 2)
      (throw 'not-found "Error: Not found")))

  (if (f-file? (concat path file))
      (prin1 (concat path file))
    (recursively-check-for-file file (go-up-dir path))))

(defun set-console-location ()
  "Set the global variable `console-location`."
  (setq console-location (recursively-check-for-file "bin/console" default-directory)))

(defvar extern-arg-list '("doctrine:query:sql"
			  "doctrine:query:dql"))

(defun execute-command (command)
  "Execute the given COMMAND with the Symfony console.
If the command requires external input, then ask for some in the minibuffer."
  (let ((cmd (nth 2(split-string command " "))))
    (async-shell-command (concat console-location " " cmd
				 (if (member cmd extern-arg-list
					     (concat " \""
						     (read-string "Please enter argument(s): ")
						     "\"")))))))

(defun create-list-of-commands ()
  "Take the ouput of `bin/console`, parse it, and return a list of commands."
  (setq unsorted-list (split-string (shell-command-to-string console-location) "\n"))

  (setq command-list '())

  (dolist (el unsorted-list)
    (if (string-match "\\w:\\w" el)
	(push el command-list)))
  (prin1 command-list))

(defun ivy-symfony-console ()
  "Show a list of the available commands in the Ivy minibuffer."
  (interactive)
  (set-console-location)
  (ivy-read "Execute command: " (reverse (create-list-of-commands)) :action #'execute-command))

(provide 'ivy-symfony-console)
;;; ivy-symfony-console.el ends here
